#!/bin/bash

# Tune things up 

# Install utilities 

sudo yum install telnet vim unzip -y

# Install docker

sudo yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2

sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

sudo yum install docker-ce-19.03.8-3.el7 docker-ce-cli-19.03.8-3.el7 containerd.io -y
sudo systemctl enable docker

sudo usermod -aG docker centos

# Install docker-compose

sudo curl -L "https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
